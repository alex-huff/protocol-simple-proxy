package com.alexfh.psp.server;

import com.alexfh.psp.PSPExecutorService;

import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocketFactory;
import java.io.*;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;

public
class PSPProxyServer
{

    private final String address;
    private final int    port;
    private final String psAddress;
    private final int    psPort;
    private final byte[] authenticationKey;

    public
    PSPProxyServer(String address, int port, String psAddress, int psPort, byte[] authenticationKey)
    {
        this.address           = address;
        this.port              = port;
        this.psAddress         = psAddress;
        this.psPort            = psPort;
        this.authenticationKey = authenticationKey;
    }

    public
    void run()
    {
        try
        {
            while (!Thread.currentThread().isInterrupted())
            {
                try
                {
                    System.out.println("starting proxy server");
                    this.runServer();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
                Thread.sleep(1000);
            }
        }
        catch (InterruptedException ignored)
        {
        }
    }

    private
    void runServer() throws IOException
    {
        ServerSocketFactory sslServerSocketFactory = SSLServerSocketFactory.getDefault();
        try (ServerSocket serverSocket = sslServerSocketFactory.createServerSocket())
        {
            serverSocket.bind(new InetSocketAddress(this.address, this.port));
            System.out.println("listening on address: " + this.address + " | port: " + this.port);
            while (!Thread.currentThread().isInterrupted())
            {
                Socket socket = serverSocket.accept();
                System.out.println(
                    "handling client: " + socket.getInetAddress().getHostAddress() + ":" + socket.getPort());
                PSPExecutorService.INSTANCE.submit(() -> this.handleClient(socket));
            }
        }
    }

    private
    void handleClient(Socket socket)
    {
        try (socket)
        {
            BufferedInputStream  bufferedInputStream          = new BufferedInputStream(socket.getInputStream());
            BufferedOutputStream bufferedOutputStream         = new BufferedOutputStream(socket.getOutputStream());
            DataInputStream      dataInputStream              = new DataInputStream(bufferedInputStream);
            DataOutputStream     dataOutputStream             = new DataOutputStream(bufferedOutputStream);
            int                  givenAuthenticationKeyLength = dataInputStream.readInt();
            if (givenAuthenticationKeyLength > 8192)
            {
                System.err.println("client authentication key too large");
                return;
            }
            byte[] givenAuthenticationKey = new byte[givenAuthenticationKeyLength];
            dataInputStream.readFully(givenAuthenticationKey);
            if (!Arrays.equals(givenAuthenticationKey, this.authenticationKey))
            {
                System.err.println("client had invalid authentication key");
                return;
            }
            int bytesPerAudioPacket = dataInputStream.readInt();
            if (bytesPerAudioPacket > 1 << 20)
            {
                System.err.println("client bytesPerAudioPacket too large");
                return;
            }
            try (Socket psSocket = new Socket(this.psAddress, this.psPort))
            {
                BufferedInputStream psBufferedInputStream = new BufferedInputStream(psSocket.getInputStream());
                byte[]              psBuffer              = new byte[bytesPerAudioPacket];
                int                 positionInAudioPacket = 0;
                int                 bytesRead;
                while (!Thread.currentThread().isInterrupted())
                {
                    while (positionInAudioPacket < bytesPerAudioPacket)
                    {
                        bytesRead = psBufferedInputStream.read(psBuffer, positionInAudioPacket,
                            bytesPerAudioPacket - positionInAudioPacket);
                        if (bytesRead < 0)
                        {
                            return;
                        }
                        dataOutputStream.write(psBuffer, positionInAudioPacket, bytesRead);
                        positionInAudioPacket += bytesRead;
                    }
                    positionInAudioPacket = 0;
                    dataOutputStream.flush();
                }
            }
        }
        catch (IOException e)
        {
            System.err.println("failed to handle client: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
