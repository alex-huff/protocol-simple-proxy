package com.alexfh.psp;

import com.alexfh.psp.server.PSPProxyServer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;
import java.util.concurrent.ExecutionException;

public
class PSP
{
    public static
    void main(String[] args) throws InterruptedException
    {
        if (args.length != 5)
        {
            System.out.println("usage: psp <listen-address> <listen-port> <remote-address> <remote-port> <authentication-key-file>");
            return;
        }
        PSPProxyServer pspProxyServer = PSP.buildPSPProxyServer(args);
        if (pspProxyServer == null)
        {
            System.err.println("failed to build PSP proxy server");
            return;
        }
        try
        {
            PSPExecutorService.INSTANCE.submit(pspProxyServer::run).get();
        }
        catch (ExecutionException e)
        {
            throw new RuntimeException(e);
        }
        System.out.println("shutting down");
    }

    private static
    PSPProxyServer buildPSPProxyServer(String[] args)
    {
        String listenAddress    = args[0];
        String listenPortString = args[1];
        String remoteAddress    = args[2];
        String remotePortString = args[3];
        int    listenPort, remotePort;
        byte[] authenticationKey;
        try
        {
            listenPort = Integer.parseInt(listenPortString);
        }
        catch (NumberFormatException ignored)
        {
            System.err.println("failed to parse listen port: " + listenPortString);
            return null;
        }
        try
        {
            remotePort = Integer.parseInt(remotePortString);
        }
        catch (NumberFormatException ignored)
        {
            System.err.println("failed to parse remote port: " + remotePortString);
            return null;
        }
        try
        {
            authenticationKey = Base64.getDecoder().decode(Files.readString(Path.of(args[4])));
        }
        catch (IOException e)
        {
            System.err.println("IOException while reading authentication key file: " + e.getMessage());
            return null;
        }
        return new PSPProxyServer(listenAddress, listenPort, remoteAddress, remotePort, authenticationKey);
    }

}