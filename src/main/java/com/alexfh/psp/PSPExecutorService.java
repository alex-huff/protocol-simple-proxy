package com.alexfh.psp;

import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public
class PSPExecutorService extends ThreadPoolExecutor
{
    public static final PSPExecutorService INSTANCE = new PSPExecutorService();

    private
    PSPExecutorService()
    {
        super(0, Integer.MAX_VALUE, 60L, TimeUnit.SECONDS, new SynchronousQueue<>());
    }
}
